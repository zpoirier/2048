#! /usr/bin/env bash
#boucle while continue tant que trouve=1 et is_full=1 (not trouve et not is_full)


taille=$(cat taille)


function trouve {
for i in $(seq 0 "$taille")
do
 for j in $(seq 0 "$taille")
 do 
  if [ $(cat matrice/l$i/c$j) = "2048" ]
  then 
    echo "0"
    return
  fi
 done
done
echo "1" 
} 


function is_full {
for i in $(seq 0 "$taille")
do
 for j in $(seq 0 "$taille")
 do 
  if [ "$(cat matrice/l$i/c$j)" = "0" ] 
  then  
    echo "1"
    return
  fi 
 done 
done 
echo "0"
} 


if [ "$(trouve)" = "0" ]
then
  echo "1"
elif [ "$(is_full)" = "0" ]
then
  echo "2"
else
  echo "0"
fi 







