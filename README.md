# 2048

## Description

Le principe consiste en l’addition successives de cases contigües représentées par des nombres pairs et égaux.
Les cases additionnées doivent nécessairement être contigües et non diagonales. Le but est d’atteindre le nombre final de 2048.

## Jouer

executer le script main.sh
