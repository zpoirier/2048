#! /usr/bin/env bash

./init_matrice.sh
./ajout2matrice.txt
./affichage

#initialisation du score
echo "0" > score

echo "$(./test_echec_reussite.sh)"

while [ "$(./test_echec_reussite.sh)" = "0" ]
do
  ./deplacements.sh "$(./demandeutilisateur)"

  ./ajout_case_2.sh
  ./affichage
  echo "score : $(cat score)"

done

if test "$(./test_echec_reussite.sh)" = "1"
then
  echo "Vous avez gagné"
else
  echo "Vous avez perdu"
fi
