#! /usr/bin/env bash


add_score() {
  score="$(( $(cat score)+$1 ))"
  echo "$score" > score
}


dir="$1"
n="$(cat taille)"

if test "$dir" = "g"
then
  for i in $(seq 0 "$n")
  do
    for j in $(seq 1 "$n")
    do
      case_deplacee="$(cat matrice/l$i/c$j)"
      if test "$case_deplacee" != "0"
      then
        indice_nouvelle_case="$j"
        a_fusionnee="0"

        for indice_case_testee in $(seq "$(( $j-1 ))" -1 0)
        do
          #echo "$i $j $indice_case_testee"
          case_testee="$(cat matrice/l$i/c$indice_case_testee)"
          if test "$case_testee" = "0"
          then
            indice_nouvelle_case="$indice_case_testee"
          elif test "$case_testee" = "$case_deplacee"
          then
            a_fusionnee="1"
            indice_nouvelle_case="$indice_case_testee"
          else
            #echo "break"
            break
          fi
        done
        if test "$a_fusionnee" = "1"
        then
          echo "0" > "matrice/l$i/c$j"
          echo "$(( $case_deplacee * 2 ))" > "matrice/l$i/c$indice_nouvelle_case"
          add_score "$(( $case_deplacee * 2 ))"
          #echo "fusion  valeur: $case_deplacee dans $i $indice_nouvelle_case"
        else
           echo "0" > "matrice/l$i/c$j"
           echo "$case_deplacee" > "matrice/l$i/c$indice_nouvelle_case"
           #echo "deplacement valeur: $case_deplacee dans $i $indice_nouvelle_case"
        fi
      fi
    done
  done
fi

if test "$dir" = "d"
then
  for i in $(seq 0 "$n")
  do
    for j in $(seq $(( "$n"-1 )) -1 0)
    do
      case_deplacee="$(cat matrice/l$i/c$j)"
      if test "$case_deplacee" != "0"
      then
        indice_nouvelle_case="$j"
        a_fusionnee="0"

        for indice_case_testee in $(seq "$(( $j+1 ))" "$n")
        do
          #echo "$i $j $indice_case_testee"
          case_testee="$(cat matrice/l$i/c$indice_case_testee)"
          if test "$case_testee" = "0"
          then
            indice_nouvelle_case="$indice_case_testee"
          elif test "$case_testee" = "$case_deplacee"
          then
            a_fusionnee="1"
            indice_nouvelle_case="$indice_case_testee"
          else
            #echo "break"
            break
          fi
        done
        if test "$a_fusionnee" = "1"
        then
          echo "0" > "matrice/l$i/c$j"
          echo "$(( $case_deplacee * 2 ))" > "matrice/l$i/c$indice_nouvelle_case"
          add_score "$(( $case_deplacee * 2 ))"
          #echo "fusion  valeur: $case_deplacee dans $i $indice_nouvelle_case"
        else
           echo "0" > "matrice/l$i/c$j"
           echo "$case_deplacee" > "matrice/l$i/c$indice_nouvelle_case"
           #echo "deplacement valeur: $case_deplacee dans $i $indice_nouvelle_case"
        fi
      fi
    done
  done
fi

if test "$dir" = "h"
then
  for j in $(seq 0 "$n")
  do
    for i in $(seq 1 "$n")
    do
      case_deplacee="$(cat matrice/l$i/c$j)"
      if test "$case_deplacee" != "0"
      then
        indice_nouvelle_case="$i"
        a_fusionnee="0"

        for indice_case_testee in $(seq "$(( $i-1 ))" -1 0)
        do
          #echo "$i $j $indice_case_testee"
          case_testee="$(cat matrice/l$indice_case_testee/c$j)"
          if test "$case_testee" = "0"
          then
            indice_nouvelle_case="$indice_case_testee"
          elif test "$case_testee" = "$case_deplacee"
          then
            a_fusionnee="1"
            indice_nouvelle_case="$indice_case_testee"
          else
            #echo "break"
            break
          fi
        done
        if test "$a_fusionnee" = "1"
        then
          echo "0" > "matrice/l$i/c$j"
          echo "$(( $case_deplacee * 2 ))" > "matrice/l$indice_nouvelle_case/c$j"
          add_score "$(( $case_deplacee * 2 ))"
          #echo "fusion  valeur: $case_deplacee dans $indice_nouvelle_case $j"
        else
           echo "0" > "matrice/l$i/c$j"
           echo "$case_deplacee" > "matrice/l$indice_nouvelle_case/c$j"
           #echo "deplacement valeur: $case_deplacee dans $indice_nouvelle_case $j"
        fi
      fi
    done
  done
fi


if test "$dir" = "b"
then
  for j in $(seq 0 "$n")
  do
    for i in $(seq "$(( $n-1 ))" -1 0)
    do
      case_deplacee="$(cat matrice/l$i/c$j)"
      if test "$case_deplacee" != "0"
      then
        indice_nouvelle_case="$i"
        a_fusionnee="0"

        for indice_case_testee in $(seq "$(( $i+1 ))"  "$n")
        do
          #echo "$i $j $indice_case_testee"
          case_testee="$(cat matrice/l$indice_case_testee/c$j)"
          if test "$case_testee" = "0"
          then
            indice_nouvelle_case="$indice_case_testee"
          elif test "$case_testee" = "$case_deplacee"
          then
            a_fusionnee="1"
            indice_nouvelle_case="$indice_case_testee"
          else
            #echo "break"
            break
          fi
        done
        if test "$a_fusionnee" = "1"
        then
          echo "0" > "matrice/l$i/c$j"
          echo "$(( $case_deplacee * 2 ))" > "matrice/l$indice_nouvelle_case/c$j"
          add_score "$(( $case_deplacee * 2 ))"
          #echo "fusion  valeur: $case_deplacee dans $indice_nouvelle_case $j"
        else
           echo "0" > "matrice/l$i/c$j"
           echo "$case_deplacee" > "matrice/l$indice_nouvelle_case/c$j"
           #echo "deplacement valeur: $case_deplacee dans $indice_nouvelle_case $j"
        fi
      fi
    done
  done
fi


if test "$dir" = "autre"
then
  echo "mauvaise commande"
fi
