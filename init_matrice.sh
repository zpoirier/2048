#! /usr/bin/env bash


# demande la taille de la matrice n

read -p "entrez la taille de la matrice : " n
n=$(( n-1 ))
echo "$n" > taille

# vide le dossier matrice (pas obligatoire)

if test -d "matrice"
then
  rm -r matrice
fi

mkdir matrice


# initialise la matrice n x n

for i in $(seq 0 "$n")
do
  mkdir "matrice/l$i"
  for j in $(seq 0 "$n")
  do
    echo "0" > "matrice/l$i/c$j"
  done
done
